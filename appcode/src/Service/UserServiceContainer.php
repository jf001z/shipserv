<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 02/06/2018
 * Time: 12:07
 */

namespace App\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

class UserServiceContainer
{
    private $container;
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function checkApiKey($apiKey){
        return $this->container->get('doctrine')->getRepository('App:User')->checkApiKey($apiKey);
    }

    public function getUserBooking($apiKey){
        $response = array('status'=>'Failed','message'=>'Please use your API Key','orders'=>array());
        if(empty($apiKey)){
            return $response;
        }
        $orders = $this->container->get('doctrine')->getRepository('App:Order')->getUserBookingByAPI($apiKey);
        if(empty($orders)){
            $response['status'] = 'OK';
            $response['message'] = 'No booking found' ;
            return $response;
        }
        $response['status'] = 'OK';
        $response['message'] = count($orders). ' booking found' ;
        $response['orders'] = $orders;
        return $response;
    }

    public function submitQuotation($quotation,$apiKey){
        if(!$this->checkApiKey($apiKey)){
            return array('status'=>'Failed','message'=>'User not exist');
        }
        if(!array_key_exists('weight',$quotation) || !array_key_exists('capacity',$quotation) || !array_key_exists('departure',$quotation) || !array_key_exists('destination',$quotation) || !array_key_exists('start_time',$quotation)) {
            return array('status' => 'Failed', 'message' => 'Not enough information provided');
        }
        $user = $this->container->get('doctrine')->getRepository('App:User')->getUserByApiKey($apiKey);
        $this->container->get('doctrine')->getRepository('App:Order')->saveQuotationOrder($user,$quotation);
        $qutations = $this->container->get('doctrine')->getRepository('App:CarrierService')->getQuotations($quotation);

        return array('status'=>'OK','quotations'=>$qutations);
    }

    public function getUserByName($name){
        return $this->container->get('doctrine')->getRepository('App:User')->findOneBy(array('name'=>$name));
    }

    public function getUserById($id){
        $user = $this->container->get('doctrine')->getRepository('App:User')->findOneBy(array('userId'=>$id));
        return array(
            'userId'=>$user->getUserId(),
            'name'=>$user->getName(),
            'email'=>$user->getEmail(),
            'address'=>$user->getAddress(),
            'tel'=>$user->getTelephone(),
            'api'=>$user->getApiKey()
        );
    }

}
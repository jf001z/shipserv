<?php
/**
 * Created by PhpStorm.
 * User: jzhang
 * Date: 31/05/2018
 * Time: 12:22
 */

namespace App\Controller\web;


use App\Entity\Order;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="default")
     * @method("POST,GET")
     */
    public function indexAction(Request $request){

        $cookies = $request->cookies;

        if($request->getMethod() == 'POST') {
            $name = $request->request->get('username');
            $password = $request->request->get('password');
            if(empty($name)||empty($password)){
                return $this->redirectToRoute('default',array(),302);
            }
            if(!$cookies->has('JSESSIONID')){
                return $this->redirectToRoute('default',array(),302);
            }
            $sessionKey = $cookies->get('JSESSIONID');
            $user = $this->get('user_service_container')->getUserByName($name);
            if(empty($user)){
                return $this->redirectToRoute('default',array(),302);
            }
            if(md5($password.$user->getSalt()) == $user->getPassword()){
                $request->getSession()->set($sessionKey,$user->getUserId());
                return $this->redirectToRoute('user_info',array(),302);
            }else{
                $cookies->remove('JSESSIONID');
                return $this->redirectToRoute('default',array(),302);
            }

        }else{
            if($cookies->has('JSESSIONID')){
                $seesionKey = $cookies->get('JSESSIONID');
                if($request->getSession()->has($seesionKey)){
                    $orderApi = $request->getSession()->get($seesionKey);
                    if($this->get('user_service_container')->checkApiKey($orderApi)){
                        $this->redirectToRoute('/admin/user',array(),302);
                    }else{
                        $cookies->remove('JSESSIONID');
                    }
                }else{
                    $cookies->remove('JSESSIONID');
                }
            }
            $sess_id = bin2hex(random_bytes(16));
            $response = new Response($this->renderView('login.html.twig',array('title'=>'login')),200,array());
            $response->headers->setCookie(new Cookie('JSESSIONID',$sess_id,time() + 1200));
            return $response;
        }
    }
    /**
     * @Route("/admin/user", name="user_info")
     */
    public function userInfoAction(Request $request){
        $sessionKey = $request->cookies->get('JSESSIONID');
        if(empty($sessionKey)){
            return $this->redirectToRoute('default',array(),302);
        }
        $userId = $request->getSession()->get($sessionKey);
        $user = $this->get('user_service_container')->getUserById($userId);

        $orders = $this->get('user_service_container')->getUserBooking($user['api']);

        $response = new Response(
            $this->renderView('userinfo.html.twig',array('title'=>'userinfo','user'=>$user,'orders'=>$orders)),200,array()
        );
        return $response;
    }
}
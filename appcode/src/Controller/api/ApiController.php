<?php
/**
 * Created by PhpStorm.
 * User: jzhang
 * Date: 31/05/2018
 * Time: 12:35
 */

namespace App\Controller\api;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Flex\Response;

class ApiController extends Controller
{
    /**
     * @Route("/api/test", name="api_test")
     * @method("POST")
     */

    public function testAction(Request $request){
        return new JsonResponse($tt);
    }
    /**
     * @Route("/api/quotation", name="api_quotation")
     * @method("POST")
     * User book new order
     */
    public function quotaionAction(Request $request){

        if(!$request->request->has('api_key') && !$request->query->has('api_key')){
            $response = $this->get('user_service_container')->getUserBooking(null);
            return new JsonResponse($response);
        }
        if($request->getMethod() != 'POST'){
            return new JsonResponse(array('status'=>'Failed','message'=>'Please user POST'));
        }
        if(!$request->request->has('quotation') || empty($request->request->get('quotation'))){
            return new JsonResponse(array('status'=>'Failed','message'=>'No quotation submitted'));
        }

        $quotation = json_decode($request->request->get('quotation'),true);
        $apiKey = empty($request->request->get('api_key')) ? $request->query->get('api_key'):$request->request->get('api_key');
        $result = $this->get('user_service_container')->submitQuotation($quotation,$apiKey);
        return new JsonResponse($result);
    }
    /**
     * @Route("/api/booking", name="api_booking")
     * @method("GET")
     * Checking user current order status
     */
    public function bookingAction(Request $request){
        if(!$request->query->has('api_key')){
            $response = $this->get('user_service_container')->getUserBooking(null);
            return new JsonResponse($response);
        }
        $apiKey = $request->query->get('api_key');
        $tt = $this->get('user_service_container')->getUserBooking($apiKey);

        return new JsonResponse($tt);
    }
    /**
     * @Route("/api/check_quotation", name="api_checking_quotation")
     * @method("GET")
     * Carrier check current quotations
     */
    public function checkQuotationAction(Request $request){
        $tt = array('status'=>'ok','user'=>array('id'=>1,'name'=>'test'));
        return new JsonResponse($tt);
    }
    /**
     * @Route("/api/confirm_quotation", name="api_confirm_quotation")
     * @method("POST")
     * Carrier confirm current quotations
     */
    public function confirmQuotationAction(Request $request){
        $tt = array('status'=>'ok','user'=>array('id'=>1,'name'=>'test'));
        return new JsonResponse($tt);
    }
}
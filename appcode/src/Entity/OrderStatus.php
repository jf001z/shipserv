<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 31/05/2018
 * Time: 01:56
 */

namespace App\Entity;


class OrderStatus
{
    private $orderStatusId;
    private $status;

    /**
     * @return mixed
     */
    public function getOrderStatusId()
    {
        return $this->orderStatusId;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 02/06/2018
 * Time: 07:11
 */

namespace App\Entity;


class Country
{
    private $countryId;
    private $countryCode;
    private $countryName;
    private $localTransportTime;

    /**
     * @return mixed
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * @return mixed
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param mixed $countryCode
     */
    public function setCountryCode($countryCode): void
    {
        $this->countryCode = $countryCode;
    }

    /**
     * @return mixed
     */
    public function getCountryName()
    {
        return $this->countryName;
    }

    /**
     * @param mixed $countryName
     */
    public function setCountryName($countryName): void
    {
        $this->countryName = $countryName;
    }

    /**
     * @return mixed
     */
    public function getLocalTransportTime()
    {
        return $this->localTransportTime;
    }

    /**
     * @param mixed $localTransportTime
     */
    public function setLocalTransportTime($localTransportTime): void
    {
        $this->localTransportTime = $localTransportTime;
    }


}
<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 31/05/2018
 * Time: 01:03
 */

namespace App\Entity;


class UserRole
{
    private $roleId;
    private $name;

    /**
     * @return mixed
     */
    public function getRoleId()
    {
        return $this->roleId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}
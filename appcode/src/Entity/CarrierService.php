<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 02/06/2018
 * Time: 07:02
 */

namespace App\Entity;


class CarrierService
{
    private $carrierServiceId;
    private $carrierId;
    private $destination;
    private $departure;
    private $startTime;
    private $duration;
    private $unitPrice;
    private $active;

    /**
     * @return mixed
     */
    public function getCarrierServiceId()
    {
        return $this->carrierServiceId;
    }

    /**
     * @return mixed
     */
    public function getCarrierId()
    {
        return $this->carrierId;
    }

    /**
     * @param mixed $carrierId
     */
    public function setCarrierId($carrierId): void
    {
        $this->carrierId = $carrierId;
    }

    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param mixed $destination
     */
    public function setDestination($destination): void
    {
        $this->destination = $destination;
    }

    /**
     * @return mixed
     */
    public function getDeparture()
    {
        return $this->departure;
    }

    /**
     * @param mixed $departure
     */
    public function setDeparture($departure): void
    {
        $this->departure = $departure;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param mixed $startTime
     */
    public function setStartTime($startTime): void
    {
        $this->startTime = $startTime;
    }

    /**
     * @return mixed
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * @param mixed $duration
     */
    public function setDuration($duration): void
    {
        $this->duration = $duration;
    }

    /**
     * @return mixed
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param mixed $unitPrice
     */
    public function setUnitPrice($unitPrice): void
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

}
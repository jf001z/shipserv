<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 31/05/2018
 * Time: 00:59
 */

namespace App\Entity;


use Doctrine\ORM\EntityRepository;

class CarrierServiceRepository extends EntityRepository
{
    public function getQuotations($quotation){
        $em = $this->getEntityManager();
        $dq = $em->createQueryBuilder();
        $startTime = new \DateTime($quotation['start_time']);
        $dq->select('u')
            ->from('App:CarrierService','u')
            ->where('u.departure=:departure')
            ->andWhere('u.destination=:destination')
            ->andWhere('u.startTime>=:startTime')
            ->andWhere('u.active=1');
        $dq->setParameters(array(
            'departure'=>$quotation['departure'],
            'destination'=>$quotation['destination'],
            'startTime'=>$startTime,
        ));
        $query = $dq->getQuery();
        $quotations = $query->getResult();
        if(empty($quotations)){
            return null;
        }
        $returnQuotation = array();
        foreach ($quotations as $q){
            $tempQuotaion = array();
            $tempQuotaion['unit_price'] = $q->getUnitPrice();
            $tempQuotaion['duration'] = $q->getDuration();
            $tempQuotaion['start_time'] = $q->getStartTime()->format('Y-m-d');
            $carrier =  $em->getRepository('App:Carrier')->findOneBy(array('carrierId'=>$q->getCarrierId()));
            $tempQuotaion['carrier'] = array(
                'name'=>$carrier->getName(),
                'email'=>$carrier->getEmail(),
                'tel'=>$carrier->getTelephone()
            );
            $returnQuotation[] = $tempQuotaion;
        }
        return $returnQuotation;
    }
}
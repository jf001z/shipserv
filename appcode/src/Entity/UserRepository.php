<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 31/05/2018
 * Time: 00:59
 */

namespace App\Entity;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class UserRepository extends EntityRepository
{
    public function checkApiKey($apikey){
        $result = $this->findBy(array('apiKey'=>$apikey));
        return !empty($result);
    }
    public function getUserByApiKey($apikey){
        $result = $this->findOneBy(array('apiKey'=>$apikey));
        return $result;
    }
}
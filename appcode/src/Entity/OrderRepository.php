<?php
/**
 * Created by PhpStorm.
 * User: jinfeizhang
 * Date: 31/05/2018
 * Time: 00:59
 */

namespace App\Entity;


use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class OrderRepository extends EntityRepository
{
    public function getUserBookingByAPI($apikey){
        $em = $this->getEntityManager();
        $dq = $em->createQueryBuilder();
        $dq->select('o')
            ->from('App:User','u')
            ->join('App:Order','o','WITH','u.userId=o.userId')
            ->where('u.apiKey=:userapi');
        $dq->setParameters(array(
            'userapi'=>$apikey
        ));
        $query = $dq->getQuery();
        $orders = $query->getResult();
        if(empty($orders)){
            return null;
        }
        $return_orders = array();
        foreach ($orders as $o){
            $tempOrder = array();
            $tempOrder['order_id'] = $o->getOrderId();
            $tempOrder['weight'] = $o->getWeight();
            $tempOrder['price'] = $o->getPrice();
            $tempOrder['capacity'] = $o->getCapacity();
            $tempOrder['departure'] = $o->getDeparture();
            $tempOrder['destination'] = $o->getDestination();
            $statusId = $o->getStatusId();
            $orderstatus = $em->getRepository('App:OrderStatus')->findOneBy(array('orderStatusId'=>$statusId));
            $tempOrder['order_status'] = $orderstatus->getStatus();
            $carrierId = $o->getCarrierId();
            if($carrierId > 0){
                $carrier_service = $em->getRepository('App:CarrierService')->findOneBy(array('carrierServiceId'=>$carrierId));
                $carrier = $em->getRepository('App:Carrier')->findOneBy(array('carrierId'=>$carrier_service->getCarrierId()));
                $tempCarrier =
                    array('name'=>$carrier->getName(),
                        'email'=>$carrier->getEmail(),
                        'address'=>$carrier->getAddress(),
                        'telephone'=>$carrier->getTelephone()
                    );
                $tempCarrierService = array();
                $tempCarrierService['carrier'] = $tempCarrier;
                $tempCarrierService['departure'] = $carrier_service->getDeparture();
                $tempCarrierService['destination'] = $carrier_service->getDestination();
                $tempCarrierService['start_time'] = $carrier_service->getStartTime()->format('Y-m-d');
                $tempCarrierService['duration'] = $carrier_service->getDuration();
                $tempCarrierService['unit_price'] = $carrier_service->getUnitPrice();
                $tempOrder['carrier_service'] = $tempCarrierService;
            }else{
                $tempOrder['carrier_service'] = null;
            }
            $return_orders[] = $tempOrder;
        }
        return $return_orders;
    }
    public function saveQuotationOrder($user,$quotation){
        $order = new Order();
        $order->setCapacity($quotation['capacity']);
        $order->setCarrierId(0);
        $order->setPrice(0);
        $order->setStatusId(1);
        $order->setDeparture($quotation['departure']);
        $order->setDestination($quotation['destination']);
        $order->setUserId($user->getUserId());
        $order->setWeight($quotation['weight']);

        $em = $this->getEntityManager();
        $em->persist($order);
        $em->flush();
    }
}
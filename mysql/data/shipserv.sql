# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 0.0.0.0 (MySQL 5.7.10)
# Database: shipserv
# Generation Time: 2018-06-04 08:32:27 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table carrier
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carrier`;

CREATE TABLE `carrier` (
  `carrier_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `api_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`carrier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `carrier` WRITE;
/*!40000 ALTER TABLE `carrier` DISABLE KEYS */;

INSERT INTO `carrier` (`carrier_id`, `name`, `email`, `address`, `telephone`, `password`, `salt`, `api_key`)
VALUES
	(1,'carrier1','carrier1@shipserv.com','TEST','09494837582','770981a43c73a8e2ba82f321793c75a6','N6x0BpBs5h','JyADYsgNN7FfA26TTakhtTSF5T4ITMu4'),
	(2,'carrier2','carrier2@shipserv.com','TEST','09494837582','770981a43c73a8e2ba82f321793c75a6','N6x0BpBs5h','GGz1Gur8qRV1FHbTh8ELYCmvg5c2rk56'),
	(3,'carrier3','carrier3@shipserv.com','TEST','09494837582','770981a43c73a8e2ba82f321793c75a6','N6x0BpBs5h','y9ZJO8c8mc4lSvjV9GoG7pGjyLSmopN7');

/*!40000 ALTER TABLE `carrier` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table carrier_service
# ------------------------------------------------------------

DROP TABLE IF EXISTS `carrier_service`;

CREATE TABLE `carrier_service` (
  `carrier_service_id` int(11) NOT NULL AUTO_INCREMENT,
  `carrier_id` int(11) NOT NULL,
  `departure` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `destination` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `start_time` datetime NOT NULL,
  `unit_price` decimal(10,0) NOT NULL,
  `duration` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  PRIMARY KEY (`carrier_service_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `carrier_service` WRITE;
/*!40000 ALTER TABLE `carrier_service` DISABLE KEYS */;

INSERT INTO `carrier_service` (`carrier_service_id`, `carrier_id`, `departure`, `destination`, `start_time`, `unit_price`, `duration`, `active`)
VALUES
	(1,1,'CN','GB','2018-11-01 00:00:00',200,90,1),
	(2,1,'CN','GB','2018-06-01 00:00:00',200,90,1),
	(3,1,'CN','GB','2018-07-01 00:00:00',200,90,1),
	(4,1,'CN','GB','2018-08-01 00:00:00',200,90,1),
	(5,1,'CN','GB','2018-09-01 00:00:00',200,90,1),
	(6,1,'CN','GB','2018-10-01 00:00:00',220,90,1),
	(7,1,'GB','CN','2018-06-15 00:00:00',220,90,1),
	(8,1,'GB','CN','2018-07-15 00:00:00',220,90,1),
	(9,1,'GB','CN','2018-08-15 00:00:00',220,90,1),
	(10,1,'GB','CN','2018-09-15 00:00:00',220,90,1),
	(11,1,'GB','CN','2018-10-15 00:00:00',220,90,1),
	(12,1,'US','GB','2018-06-12 00:00:00',100,30,1),
	(14,1,'US','GB','2018-07-12 00:00:00',100,30,1),
	(16,1,'US','GB','2018-08-12 00:00:00',100,30,1),
	(18,1,'US','GB','2018-09-12 00:00:00',100,30,1),
	(20,1,'GB','US','2018-06-02 00:00:00',80,30,1),
	(22,1,'GB','US','2018-07-02 00:00:00',80,30,1),
	(24,1,'GB','US','2018-08-02 00:00:00',80,30,1),
	(26,1,'GB','US','2018-09-02 00:00:00',80,30,1),
	(55,2,'CN','GB','2018-11-10 00:00:00',180,102,1),
	(56,2,'CN','GB','2018-06-10 00:00:00',180,102,1),
	(57,2,'CN','GB','2018-07-10 00:00:00',180,102,1),
	(58,2,'CN','GB','2018-08-10 00:00:00',180,102,1),
	(59,2,'CN','GB','2018-09-10 00:00:00',180,102,1),
	(60,2,'CN','GB','2018-10-10 00:00:00',180,102,1),
	(61,2,'GB','CN','2018-06-20 00:00:00',190,102,1),
	(62,2,'GB','CN','2018-07-20 00:00:00',190,102,1),
	(63,2,'GB','CN','2018-08-20 00:00:00',190,102,1),
	(64,2,'GB','CN','2018-09-20 00:00:00',190,102,1),
	(65,2,'GB','CN','2018-10-20 00:00:00',190,102,1),
	(66,2,'US','GB','2018-06-01 00:00:00',90,35,1),
	(68,2,'US','GB','2018-07-01 00:00:00',90,35,1),
	(70,2,'US','GB','2018-08-01 00:00:00',90,35,1),
	(72,2,'US','GB','2018-09-01 00:00:00',90,35,1),
	(75,2,'GB','US','2018-06-30 00:00:00',70,35,1),
	(77,2,'GB','US','2018-07-30 00:00:00',70,35,1),
	(79,2,'GB','US','2018-08-30 00:00:00',70,35,1),
	(81,2,'GB','US','2018-09-30 00:00:00',70,35,1),
	(82,3,'CN','GB','2018-11-30 00:00:00',220,80,1),
	(83,3,'CN','GB','2018-06-30 00:00:00',220,80,1),
	(84,3,'CN','GB','2018-07-30 00:00:00',220,80,1),
	(85,3,'CN','GB','2018-08-30 00:00:00',220,80,1),
	(86,3,'CN','GB','2018-09-30 00:00:00',220,80,1),
	(87,3,'CN','GB','2018-10-30 00:00:00',220,80,1),
	(88,3,'GB','CN','2018-06-01 00:00:00',240,80,1),
	(89,3,'GB','CN','2018-07-01 00:00:00',240,80,1),
	(90,3,'GB','CN','2018-08-01 00:00:00',240,80,1),
	(91,3,'GB','CN','2018-09-01 00:00:00',240,80,1),
	(92,3,'GB','CN','2018-10-01 00:00:00',240,80,1),
	(93,3,'US','GB','2018-06-15 00:00:00',90,35,1),
	(94,3,'US','GB','2018-07-15 00:00:00',90,35,1),
	(95,3,'US','GB','2018-08-15 00:00:00',90,35,1),
	(96,3,'US','GB','2018-09-15 00:00:00',90,35,1),
	(97,3,'GB','US','2018-06-29 00:00:00',70,35,1),
	(98,3,'GB','US','2018-07-29 00:00:00',70,35,1),
	(99,3,'GB','US','2018-08-29 00:00:00',70,35,1),
	(100,3,'GB','US','2018-09-29 00:00:00',70,35,1),
	(101,1,'CN','GB','2018-05-01 00:00:00',200,90,1),
	(102,1,'CN','GB','2018-04-01 00:00:00',200,90,1),
	(103,1,'CN','GB','2018-03-01 00:00:00',200,90,1),
	(104,1,'CN','GB','2018-02-01 00:00:00',200,90,1),
	(105,1,'CN','GB','2018-01-01 00:00:00',200,90,1),
	(106,2,'US','GB','2018-01-01 00:00:00',90,35,1),
	(107,2,'US','GB','2018-02-01 00:00:00',90,35,1),
	(108,2,'US','GB','2018-03-01 00:00:00',90,35,1),
	(109,2,'US','GB','2018-04-01 00:00:00',90,35,1),
	(110,2,'US','GB','2018-05-01 00:00:00',90,35,1),
	(111,2,'CN','GB','2018-01-10 00:00:00',180,102,1),
	(112,2,'CN','GB','2018-02-10 00:00:00',180,102,1),
	(113,2,'CN','GB','2018-03-10 00:00:00',180,102,1),
	(114,2,'CN','GB','2018-04-10 00:00:00',180,102,1),
	(115,2,'CN','GB','2018-05-10 00:00:00',180,102,1);

/*!40000 ALTER TABLE `carrier_service` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table country
# ------------------------------------------------------------

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `country_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `local_transport_time` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;

INSERT INTO `country` (`id`, `country_code`, `country_name`, `local_transport_time`)
VALUES
	(1,'ZW','Zimbabwe',5),
	(2,'AL','Albania',5),
	(3,'DZ','Algeria',5),
	(4,'DS','American Samoa',5),
	(5,'AD','Andorra',5),
	(6,'AO','Angola',5),
	(7,'AI','Anguilla',5),
	(8,'AQ','Antarctica',5),
	(9,'AG','Antigua and Barbuda',5),
	(10,'AR','Argentina',5),
	(11,'AM','Armenia',5),
	(12,'AW','Aruba',5),
	(13,'AU','Australia',5),
	(14,'AT','Austria',5),
	(15,'AZ','Azerbaijan',5),
	(16,'BS','Bahamas',5),
	(17,'BH','Bahrain',5),
	(18,'BD','Bangladesh',5),
	(19,'BB','Barbados',5),
	(20,'BY','Belarus',5),
	(21,'BE','Belgium',5),
	(22,'BZ','Belize',5),
	(23,'BJ','Benin',5),
	(24,'BM','Bermuda',5),
	(25,'BT','Bhutan',5),
	(26,'BO','Bolivia',5),
	(27,'BA','Bosnia and Herzegovina',5),
	(28,'BW','Botswana',5),
	(29,'BV','Bouvet Island',5),
	(30,'BR','Brazil',5),
	(31,'IO','British Indian Ocean Territory',5),
	(32,'BN','Brunei Darussalam',5),
	(33,'BG','Bulgaria',5),
	(34,'BF','Burkina Faso',5),
	(35,'BI','Burundi',5),
	(36,'KH','Cambodia',5),
	(37,'CM','Cameroon',5),
	(38,'CA','Canada',5),
	(39,'CV','Cape Verde',5),
	(40,'KY','Cayman Islands',5),
	(41,'CF','Central African Republic',5),
	(42,'TD','Chad',5),
	(43,'CL','Chile',5),
	(44,'CN','China',12),
	(45,'CX','Christmas Island',5),
	(46,'CC','Cocos (Keeling) Islands',5),
	(47,'CO','Colombia',5),
	(48,'KM','Comoros',5),
	(49,'CG','Congo',5),
	(50,'CK','Cook Islands',5),
	(51,'CR','Costa Rica',5),
	(52,'HR','Croatia (Hrvatska)',5),
	(53,'CU','Cuba',5),
	(54,'CY','Cyprus',5),
	(55,'CZ','Czech Republic',5),
	(56,'DK','Denmark',5),
	(57,'DJ','Djibouti',5),
	(58,'DM','Dominica',5),
	(59,'DO','Dominican Republic',5),
	(60,'TP','East Timor',5),
	(61,'EC','Ecuador',5),
	(62,'EG','Egypt',5),
	(63,'SV','El Salvador',5),
	(64,'GQ','Equatorial Guinea',5),
	(65,'ER','Eritrea',5),
	(66,'EE','Estonia',5),
	(67,'ET','Ethiopia',5),
	(68,'FK','Falkland Islands (Malvinas)',5),
	(69,'FO','Faroe Islands',5),
	(70,'FJ','Fiji',5),
	(71,'FI','Finland',5),
	(72,'FR','France',7),
	(73,'FX','France, Metropolitan',5),
	(74,'GF','French Guiana',5),
	(75,'PF','French Polynesia',5),
	(76,'TF','French Southern Territories',5),
	(77,'GA','Gabon',5),
	(78,'GM','Gambia',5),
	(79,'GE','Georgia',5),
	(80,'DE','Germany',5),
	(81,'GH','Ghana',5),
	(82,'GI','Gibraltar',5),
	(83,'GK','Guernsey',5),
	(84,'GR','Greece',5),
	(85,'GL','Greenland',5),
	(86,'GD','Grenada',5),
	(87,'GP','Guadeloupe',5),
	(88,'GU','Guam',5),
	(89,'GT','Guatemala',5),
	(90,'GN','Guinea',5),
	(91,'GW','Guinea-Bissau',5),
	(92,'GY','Guyana',5),
	(93,'HT','Haiti',5),
	(94,'HM','Heard and Mc Donald Islands',5),
	(95,'HN','Honduras',5),
	(96,'HK','Hong Kong',5),
	(97,'HU','Hungary',5),
	(98,'IS','Iceland',5),
	(99,'IN','India',5),
	(100,'IM','Isle of Man',5),
	(101,'ID','Indonesia',5),
	(102,'IR','Iran (Islamic Republic of)',5),
	(103,'IQ','Iraq',5),
	(104,'IE','Ireland',5),
	(105,'IL','Israel',5),
	(106,'IT','Italy',5),
	(107,'CI','Ivory Coast',5),
	(108,'JE','Jersey',5),
	(109,'JM','Jamaica',5),
	(110,'JP','Japan',5),
	(111,'JO','Jordan',5),
	(112,'KZ','Kazakhstan',5),
	(113,'KE','Kenya',5),
	(114,'KI','Kiribati',5),
	(115,'KP','Korea, Democratic People\'s Republic of',5),
	(116,'KR','Korea, Republic of',5),
	(117,'XK','Kosovo',5),
	(118,'KW','Kuwait',5),
	(119,'KG','Kyrgyzstan',5),
	(120,'LA','Lao People\'s Democratic Republic',5),
	(121,'LV','Latvia',5),
	(122,'LB','Lebanon',5),
	(123,'LS','Lesotho',5),
	(124,'LR','Liberia',5),
	(125,'LY','Libyan Arab Jamahiriya',5),
	(126,'LI','Liechtenstein',5),
	(127,'LT','Lithuania',5),
	(128,'LU','Luxembourg',5),
	(129,'MO','Macau',5),
	(130,'MK','Macedonia',5),
	(131,'MG','Madagascar',5),
	(132,'MW','Malawi',5),
	(133,'MY','Malaysia',5),
	(134,'MV','Maldives',5),
	(135,'ML','Mali',5),
	(136,'MT','Malta',5),
	(137,'MH','Marshall Islands',5),
	(138,'MQ','Martinique',5),
	(139,'MR','Mauritania',5),
	(140,'MU','Mauritius',5),
	(141,'TY','Mayotte',5),
	(142,'MX','Mexico',5),
	(143,'FM','Micronesia, Federated States of',5),
	(144,'MD','Moldova, Republic of',5),
	(145,'MC','Monaco',5),
	(146,'MN','Mongolia',5),
	(147,'ME','Montenegro',5),
	(148,'MS','Montserrat',5),
	(149,'MA','Morocco',5),
	(150,'MZ','Mozambique',5),
	(151,'MM','Myanmar',5),
	(152,'NA','Namibia',5),
	(153,'NR','Nauru',5),
	(154,'NP','Nepal',5),
	(155,'NL','Netherlands',5),
	(156,'AN','Netherlands Antilles',5),
	(157,'NC','New Caledonia',5),
	(158,'NZ','New Zealand',5),
	(159,'NI','Nicaragua',5),
	(160,'NE','Niger',5),
	(161,'NG','Nigeria',5),
	(162,'NU','Niue',5),
	(163,'NF','Norfolk Island',5),
	(164,'MP','Northern Mariana Islands',5),
	(165,'NO','Norway',5),
	(166,'OM','Oman',5),
	(167,'PK','Pakistan',5),
	(168,'PW','Palau',5),
	(169,'PS','Palestine',5),
	(170,'PA','Panama',5),
	(171,'PG','Papua New Guinea',5),
	(172,'PY','Paraguay',5),
	(173,'PE','Peru',5),
	(174,'PH','Philippines',5),
	(175,'PN','Pitcairn',5),
	(176,'PL','Poland',5),
	(177,'PT','Portugal',5),
	(178,'PR','Puerto Rico',5),
	(179,'QA','Qatar',5),
	(180,'RE','Reunion',5),
	(181,'RO','Romania',5),
	(182,'RU','Russian Federation',5),
	(183,'RW','Rwanda',5),
	(184,'KN','Saint Kitts and Nevis',5),
	(185,'LC','Saint Lucia',5),
	(186,'VC','Saint Vincent and the Grenadines',5),
	(187,'WS','Samoa',5),
	(188,'SM','San Marino',5),
	(189,'ST','Sao Tome and Principe',5),
	(190,'SA','Saudi Arabia',5),
	(191,'SN','Senegal',5),
	(192,'RS','Serbia',5),
	(193,'SC','Seychelles',5),
	(194,'SL','Sierra Leone',5),
	(195,'SG','Singapore',5),
	(196,'SK','Slovakia',5),
	(197,'SI','Slovenia',5),
	(198,'SB','Solomon Islands',5),
	(199,'SO','Somalia',5),
	(200,'ZA','South Africa',5),
	(201,'GS','South Georgia South Sandwich Islands',5),
	(202,'ES','Spain',5),
	(203,'LK','Sri Lanka',5),
	(204,'SH','St. Helena',5),
	(205,'PM','St. Pierre and Miquelon',5),
	(206,'SD','Sudan',5),
	(207,'SR','Suriname',5),
	(208,'SJ','Svalbard and Jan Mayen Islands',5),
	(209,'SZ','Swaziland',5),
	(210,'SE','Sweden',5),
	(211,'CH','Switzerland',5),
	(212,'SY','Syrian Arab Republic',5),
	(213,'TW','Taiwan',5),
	(214,'TJ','Tajikistan',5),
	(215,'TZ','Tanzania, United Republic of',5),
	(216,'TH','Thailand',5),
	(217,'TG','Togo',5),
	(218,'TK','Tokelau',5),
	(219,'TO','Tonga',5),
	(220,'TT','Trinidad and Tobago',5),
	(221,'TN','Tunisia',5),
	(222,'TR','Turkey',5),
	(223,'TM','Turkmenistan',5),
	(224,'TC','Turks and Caicos Islands',5),
	(225,'TV','Tuvalu',5),
	(226,'UG','Uganda',5),
	(227,'UA','Ukraine',5),
	(228,'AE','United Arab Emirates',5),
	(229,'GB','United Kingdom',3),
	(230,'US','United States',10),
	(231,'UM','United States minor outlying islands',5),
	(232,'UY','Uruguay',5),
	(233,'UZ','Uzbekistan',5),
	(234,'VU','Vanuatu',5),
	(235,'VA','Vatican City State',5),
	(236,'VE','Venezuela',5),
	(237,'VN','Vietnam',5),
	(238,'VG','Virgin Islands (British)',5),
	(239,'VI','Virgin Islands (U.S.)',5),
	(240,'WF','Wallis and Futuna Islands',5),
	(241,'EH','Western Sahara',5),
	(242,'YE','Yemen',5),
	(243,'ZR','Zaire',5),
	(244,'ZM','Zambia',5);

/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table customer_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer_order`;

CREATE TABLE `customer_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `carrier_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `weight` double NOT NULL,
  `capacity` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `departure` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `customer_order` WRITE;
/*!40000 ALTER TABLE `customer_order` DISABLE KEYS */;

INSERT INTO `customer_order` (`order_id`, `carrier_id`, `user_id`, `status_id`, `weight`, `capacity`, `price`, `departure`, `destination`)
VALUES
	(1,111,1,4,1000,10,1800,'CN','GB'),
	(2,103,1,4,1500,20,4000,'CN','GB'),
	(3,114,1,4,1500,10,1800,'CN','GB'),
	(4,0,1,1,1000,25,0,'CN','GB'),
	(5,0,1,1,1000,25,0,'CN','GB'),
	(6,0,1,1,1000,25,0,'CN','GB'),
	(7,-1,1,1,1000,25,0,'GB','US'),
	(8,-1,1,1,1000,25,0,'GB','US');

/*!40000 ALTER TABLE `customer_order` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migration_versions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migration_versions`;

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



# Dump of table order_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order_status`;

CREATE TABLE `order_status` (
  `order_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`order_status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `order_status` WRITE;
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;

INSERT INTO `order_status` (`order_status_id`, `status`)
VALUES
	(1,'quotation'),
	(2,'booked'),
	(3,'on the way'),
	(4,'delivered'),
	(5,'paid'),
	(6,'rejected');

/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `salt` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `api_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`user_id`, `name`, `role_id`, `email`, `address`, `telephone`, `password`, `salt`, `api_key`)
VALUES
	(1,'user1',2,'user1@shipserv.com','test','09494837582','770981a43c73a8e2ba82f321793c75a6','N6x0BpBs5h','8l27Nwkh73NwUSmX4R19QWIbeutjZVRL'),
	(2,'user2',2,'user2@shipserv.com','test','09494837582','770981a43c73a8e2ba82f321793c75a6','N6x0BpBs5h','f6XhxoFxFuXxshNoBCblcjgYQEG3cvcc'),
	(3,'user3',2,'user3@shipserv.com','test','09494837582','770981a43c73a8e2ba82f321793c75a6','N6x0BpBs5h','bfyhPHVM8aP3hNKKEarJkNWiXkr7C04Y');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_role
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;

INSERT INTO `user_role` (`role_id`, `name`)
VALUES
	(1,'admin'),
	(2,'customer');

/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

# README #


### What is this repository for? ###

This repository is for the challenge of the job application for Shipserv.
    
### Request Analysis ###

There is not enough information provided in the challenge description document. 
The only requirement is to create an API endpoint for customer to check their booking 
with simple UI.

Based on the anaylsis of the compony's backgound, I setup the enviroment as follow:

This is a customer service web application for a marine service agency. The agency 
build up an interface between marine shipping companies and customers. It has
various marine shipping companies in its database. Customers can send quotations to the
company and check their previous booking.

### How do I get set up? ###

In Linux or Mac system, please run the following command:
```
$ git clone https://bitbucket.org/jf001z/shipserv.git
$ cd shipserv
$ docker-compose up -d
``` 
After the setup completed, please wait 5 mins for nginx and php-pfm ready to work.

### Customer UI ###

After the application deployed, go to:
```angular2html
http://localhost
```
Then use the following account to login:
account: user1
password: 12345

### Access to Database ###

database host: 127.0.0.1:3306

user: admin

password: shipserv

database: shipserv

### API Endpoint ###

Two endpoints has been created:

* Quotation: Customer can send their quotations to this endpoit and suitable shipping service will be
send back to the customer 

    * Method: POST

    * URL: 
        ```angular2html
        localhost/api/quotation
        ```

    * Two key-value are reuqired, Here is an example:

        aip_key: 8l27Nwkh73NwUSmX4R19QWIbeutjZVRL

        quotation: {
               "weight": 1000,
               "capacity": 30,
               "departure": "CN",
               "destination": "GB",
               "start_time": "2018-06-20"
           }
           
* Check current and history bookings:
    
    * Method: GET
    
    * URL: 
        ```
                localhost/api/booking?api_key=8l27Nwkh73NwUSmX4R19QWIbeutjZVRL
        ```
      Different users have different api_keys. This is the key for user1
    * Data Sample:
        ```
        {
            "status": "OK",
            "message": "3 booking found",
            "orders": [
                {
                    "order_id": 1,
                    "weight": 1000,
                    "price": "1800",
                    "capacity": 10,
                    "order_status": "delivered",
                    "carrier_service": {
                        "carrier": {
                            "name": "carrier2",
                            "email": "carrier2@shipserv.com",
                            "address": "TEST",
                            "telephone": "09494837582"
                        },
                        "departure": "CN",
                        "destination": "GB",
                        "start_time": {
                            "date": "2018-01-10 00:00:00.000000",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        },
                        "duration": 102,
                        "unit_price": "180"
                    }
                },
                {
                    "order_id": 2,
                    "weight": 1500,
                    "price": "4000",
                    "capacity": 20,
                    "order_status": "delivered",
                    "carrier_service": {
                        "carrier": {
                            "name": "carrier1",
                            "email": "carrier1@shipserv.com",
                            "address": "TEST",
                            "telephone": "09494837582"
                        },
                        "departure": "CN",
                        "destination": "GB",
                        "start_time": {
                            "date": "2018-03-01 00:00:00.000000",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        },
                        "duration": 90,
                        "unit_price": "200"
                    }
                },
                {
                    "order_id": 3,
                    "weight": 1500,
                    "price": "1800",
                    "capacity": 10,
                    "order_status": "delivered",
                    "carrier_service": {
                        "carrier": {
                            "name": "carrier2",
                            "email": "carrier2@shipserv.com",
                            "address": "TEST",
                            "telephone": "09494837582"
                        },
                        "departure": "CN",
                        "destination": "GB",
                        "start_time": {
                            "date": "2018-04-10 00:00:00.000000",
                            "timezone_type": 3,
                            "timezone": "UTC"
                        },
                        "duration": 102,
                        "unit_price": "180"
                    }
                }
            ]
        }
        ```
